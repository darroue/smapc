@settings = {
  node_name: ENV["computername"],
  node_guid: registry_query("HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Cryptography", "MachineGuid").last.split(/\s+/).last,
  client_name: "SMAPC",
  node_auth_id: nil,
  package_file: "packages.config",
  config_file: "settings.json",
  keep_settings: false,
  skip_app_list: false,
  version: read_from_file(File.join(__dir__, "version")).split("\n").first.chomp,
  packages_to_install: [],
  packages_to_upgrade: [],
  packages_to_uninstall: {
    chocolatey: [],
    other: [],
  },
  forced_packages: [],
  installed_packages: {
    chocolatey: [],
    other: [],
  },
  logs: [],
  errors: [],
}
