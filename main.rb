# encoding: utf-8
# Forces used encoding on file

# - LICENSE -
# SMAPC - Software Managing And Patching Client v#VERSION#
#
# Copyright 2019 Petr Radouš <darroue@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# Loading required files
require_relative "dependencies"
require_relative "lib/helper"
require_relative "default_settings"

# Beginning of client run counter
client_started_at = Time.now

# Changes working directory when env variable is present
if not defined?(Ocra)
  Dir.chdir(File.dirname(ENV["OCRA_EXECUTABLE"])) if ENV["OCRA_EXECUTABLE"]
end

# Write window header
message "SMAPC - Software Managing And Patching Client", :header
# Load settings from file
load_settings
# Abort script run unless settings contain requested variable
check_need_variables :server_url

# Client self update
self_update

begin
  # Register node unless node is already registered (has :node_auth_id)
  unless @settings[:node_auth_id]
    check_need_variables :invite_auth
    register_node
  end
  check_need_variables :node_auth_id
  # Gather node attributes from server (What client should do)
  get_node_attributes
  # Uninstalling client if marked for uninstall
  if @node_attributes[:uninstall]
    uninstall_self
  end
  # Install Chocolatey client unless already installed
  install_chocolatey
  # Check state of scheduled task and create/update it if needed
  update_scheduled_task
  # Save settings to file unless variable :keep_settings is present.
  @settings[:keep_settings] ? save_settings(false) : save_settings

  # Map those attributes to different fields in settings and fill needed variables
  @settings[:apps_to_install] = @node_attributes[:apps_to_install]
  @settings[:apps_to_uninstall] = @node_attributes[:apps_to_uninstall]

  if @settings[:apps_to_install] && @settings[:apps_to_uninstall]
    @settings[:packages_to_uninstall][:chocolatey] = @settings[:apps_to_uninstall].select { |app| app[:app_type] == "chocolatey" }
    @settings[:packages_to_uninstall][:other] = @settings[:apps_to_uninstall] - @settings[:packages_to_uninstall][:chocolatey]

    # Gather list of installed applications trough Chocolatey ans Windows Registry
    get_list_of_installed_apps

    @settings[:installed_packages][:chocolatey_names] = @settings[:installed_packages][:chocolatey].map do |package| package[:name].downcase end
    @settings[:packages_to_uninstall][:chocolatey_names] = @settings[:packages_to_uninstall][:chocolatey].map do |package| package[:name].downcase end
    upgradable_packages = (@settings[:installed_packages][:chocolatey_names] - @settings[:packages_to_uninstall][:chocolatey_names])
    @settings[:apps_to_install].each do |app|
      if upgradable_packages.include?(app[:package_name].downcase) && app[:version] == "latest"
        @settings[:packages_to_upgrade].push(app[:package_name])
      else
        @settings[:packages_to_install].push(app)
      end
    end

    # If there are any packages to install, then install them
    # Packages to install are in two categories. Normal and Forced.
    # ? Packages that are marked as forced will be installed in force method only once. Then saved in config file.
    if @settings[:packages_to_install].any?
      normal_install = @settings[:packages_to_install].reject { |app| app[:forced] }
      forced_install = @settings[:packages_to_install].select { |app| app[:forced] }.reject { |app|
        @settings[:forced_packages].include?(app.slice(:package_name, :version))
      }
      packages_to_install__package_names_only_list = @settings[:packages_to_install].map do |app| "   - " + app[:package_name] end

      if (normal_install + forced_install).any?
        message "Packages", :highlight
        message ["to install:", packages_to_install__package_names_only_list.join("\n")]
      end

      # Installing normal packages
      if normal_install.any?
        build_config_file_for_install(normal_install)
        message "Installing packages", :command
        external_command "choco install --confirm --no-color --limit-output --allow-downgrade --no-progress #{File.expand_path(@settings[:package_file])}"
      end

      # Installing forced packages
      if forced_install.any?
        build_config_file_for_install(forced_install)
        @settings[:forced_packages] += forced_install.map { |app| app.slice("package_name", "version") }
        message "Installing packages (force)", :command
        external_command "choco install --confirm --no-color --limit-output --allow-downgrade --no-progress --force #{File.expand_path(@settings[:package_file])}"

        @settings[:keep_settings] ? save_settings(false) : save_settings
      end
    end

    # Upgrading installed packages
    if @settings[:packages_to_upgrade].any?
      message "Updating packages", :command
      external_command "choco upgrade --confirm --no-color --limit-output --allow-downgrade --no-progress #{@settings[:packages_to_upgrade].join("\s")}"
    end

    # Uninstalling chocolatey packages marked by server for uninstall
    if @settings[:packages_to_uninstall][:chocolatey].any?
      message "Uninstalling packages - Chocolatey", :command
      external_command "choco uninstall --confirm --no-color --limit-output -y -x #{@settings[:packages_to_uninstall][:chocolatey_names].join("\s")}"
    end

    # Uninstalling other packages marked by server for uninstall (MSI only)
    if @settings[:packages_to_uninstall][:other].any?
      message "Uninstalling packages - Other", :command
      @settings[:packages_to_uninstall][:other].each do |app|
        message "- #{app[:name]}"
        external_command "msiexec /qn /x{#{app[:uninstall_string]}}"
      end
    end

    # Sending report of installed packages to server unless client is marked for uninstallation
    report_list_of_installed_apps unless @node_attributes[:uninstall]

    # Reporting status of installed packages
    unless @settings[:skip_app_list]
      message "Installed packages", :highlight
      message "Installed by Chocolatey", :nested
      message @settings[:installed_packages][:chocolatey].map(&:values).to_table, :raw
      message "Installed by other means", :nested
      message "#{@settings[:installed_packages][:other].map { |a| a.slice(:name, :version) }.map(&:values).to_table}", :raw
    else
      message "Skipping app list", :command
    end
  end

  # Reporting how long whole client run took
  message "\nDone\n[Took: #{(Time.now.to_f - client_started_at.to_f).round(2)}]"
rescue => e
  # Reporting errors if any
  errors(e)
end
