dependencies = %w(rest-client json builder text-table fileutils colorize)

dependencies.each do |dependency|
  require dependency
end
