Dir.chdir(File.dirname(File.expand_path(__FILE__)))

version_file = "version"
main_file = "main.rb"

version = File.read(version_file).split("\n").first
main_file_content = File.read(main_file).gsub("#VERSION#", version)
File.open(main_file, "w:utf-8") { |fo| fo.write(main_file_content) }

system "ocra --output #{version}.exe --no-enc #{main_file} #{version_file} default_settings.rb dependencies.rb lib\\helper.rb"
system "git checkout -f -- #{main_file}"
