# Formats message, adjust left padding
def msg(msg, prep = 2)
  if msg.kind_of?(Array)
    msg.map! do |line|
      line.rjust(msg.max.length + prep)
    end if prep
    msg = msg.join("\n")
  else
    msg = msg.to_s.rjust(msg.to_s.length + prep) if prep
  end
  return msg
end

# Formats error message
def error(message, error_type = :fatal)
  message = message.join("\n") if message.kind_of?(Array)
  case error_type
  when :fatal
    puts ("[FATAL ERROR] " + message.to_s).red
    abort
  else
    puts ("[ERROR] " + message.to_s).light_red
  end
end

# Formats message
def message(message, message_type = :info)
  case message_type
  when :info
    puts msg(message) + "\n\n"
  when :highlight
    puts "\n" + "[#{message}]".green
  when :raw
    puts message
  when :nested
    puts msg(message)
  when :command
    puts " -> " + message.yellow + "\n"
  when :header
    puts "[#{message}]".cyan + "\n"
  else
    error "Unknown message_type! - #{message_type}"
  end
end

# Replaces linux path into windows path format
def win_path(path)
  path.gsub("/", '\\')
end

# Parses json file and return content
def parse_json(content)
  begin
    JSON.parse(content, { :symbolize_names => true }).to_hash
  rescue JSON::ParserError
    error "Invalid file format - JSON"
  end
end

# Executes external applications, print and saves its output
def external_command(command, no_output = false)
  if no_output
    command_output = `#{command}`.encode("utf-8", invalid: :replace, undef: :replace, replace: "?")
  else
    system command
  end

  if $?.success?
    @settings[:logs].push(command_output) if command_output
    return command_output
  else
    message = "Command #{command} failed with code #{$?.exitstatus}"
    @settings[:errors].push(message)
    error message, :light
    return false
  end
end

# Query registry
def registry_query(key, value)
  `reg query "#{win_path key}" /v "#{value}"`.split("\n")
end

# Modifies registry
def registry_set(key, value, data)
  external_command "reg add \"#{win_path key}\" /v \"#{value}\" /d \"#{data}\" /f", :no_output
end

# Saves data to file
def save_to_file(file, content)
  FileUtils.mkdir_p(File.dirname(file))
  File.open(file, "w:utf-8") { |fo| fo.write(content) }
end

# Reads data from file
def read_from_file(file, filetype = :plain)
  if !File.exist?(file)
    error "File doesn't exist! - #{file}"
  else
    file_content = File.read(file)
  end

  case filetype
  when :json
    parse_json file_content
  when :plain
    file_content
  else
    error "Unknown filetype! - #{filetype}"
  end
end

# Saves settings as json into file specified in settings
def save_settings(only_needed_keys = true)
  keys_to_keep = %i(server_url node_auth_id keep_settings skip_app_list forced_packages)
  if only_needed_keys
    content = JSON.pretty_generate(@settings.slice(*keys_to_keep).update({ comment: "You can specify custom settings if you set keep_settings to true." }))
  else
    content = JSON.pretty_generate(@settings.select { |k, v| !%i(packages_to_install packages_to_upgrade packages_to_uninstall installed_packages logs errors).include? k })
  end
  save_to_file(@settings[:config_file], content)
end

# Loads and parses settings
def load_settings
  message "Loading settings", :command
  file_content = read_from_file(@settings[:config_file], :json)

  @settings.update(file_content)
  @settings[:forced_packages].map! { |package| package.collect { |k, v| [k.to_s, v] }.to_h }

  message "Settings", :highlight
  message @settings.slice(:server_url, :node_name, :node_guid, :node_auth_id).map { |setting, value|
    ((setting.to_s + ":").ljust(15) + value.to_s)
  }
end

# Installs chocolatey client if needed
def install_chocolatey
  unless File.exist?('C:\ProgramData\chocolatey\choco.exe')
    message "Installing dependency", :command
    `@#{win_path "%SystemRoot%/System32/WindowsPowerShell/v1.0/powershell.exe"} -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;#{win_path "%ALLUSERSPROFILE%/chocolatey/bin"}"`

    message "Dependency installed, initializing new instance.."
    sleep 3
    system "cls"
    external_command "C:/ProgramData/#{@settings[:client_name]}/client.exe"
    exit
  end
end

# Creates / updates scheduled task
def update_scheduled_task
  if !system "schtasks /query /TN \"#{@settings[:client_name]}\"  > nul 2>&1"
    message "Creating scheduled task", :command
    external_command "schtasks /create /TN \"#{@settings[:client_name]}\" /SC \"MINUTE\" /MO 30 /TR \"#{win_path "C:/ProgramData/#{@settings[:client_name]}/client.exe"}\" /RU \"System\"", :no_output
  end
end

# Removed scheduled task
def remove_scheduled_task
  message "Removing scheduled task", :command
  client_name = @settings[:client_name]
  external_command "schtasks /delete /TN \"#{@settings[:client_name]}\" /F", :no_output
end

# Generates error messages for known errors
def errors(e)
  if e.kind_of?(Errno::ECONNREFUSED)
    error ["ERROR: Connection Refused!",
           "Check settings at for possibly invalid server_url!",
           "Detail:",
           e.message]
  elsif e.kind_of?(RestClient::InternalServerError)
    ["ERROR: Internal Server Error! - Node doesn't exist!",
     "Detail:",
     e.message]
  elsif e.kind_of?(RestClient::Forbidden)
    error "ERROR: Forbidden request!"
  elsif e.kind_of?(RestClient::UnprocessableEntity)
    error "ERROR: Invalid request!"
  else
    error e
  end
end

# Uninstalls self
def uninstall_self
  message "Client marked for uninstallation by server - Uninstalling", :command
  remove_scheduled_task
  registry_set("HKLM/SOFTWARE/Microsoft/Windows/CurrentVersion/RunOnce", "Remove SMAPC", "cmd /c 'rd /s /q #{win_path Dir.pwd}'")
  message "It's been a pleasure. See you next time, if you change your mind :).", :highlight
  abort
end

# Register node on server and save its settings
def register_node
  begin
    register = RestClient.put(
      @settings[:server_url] + "/api/v1/nodes/register", {
        hostname: @settings[:node_name],
        guid: @settings[:node_guid],
        invite_auth: @settings[:invite_auth],
      }.to_json, { content_type: :json, accept: :json }
    )
    if register.code == 200
      @settings[:node_auth_id] = parse_json(register)[:auth_id]
      save_settings
    end
  rescue => e
    errors(e)
  end
end

# Queries server for node attributes (what to do)
def get_node_attributes
  begin
    message "Getting node attributes from server", :command

    response = RestClient.get(@settings[:server_url] + "/api/v1/nodes/" + @settings[:node_auth_id])

    case response.code
    when 200
      @node_attributes = parse_json response
    when 204
      error "ERROR: Node not active!"
    end
  rescue => e
    errors(e)
  end
end

# renders config file for chocolatey, so it knows what packages to install
def build_config_file_for_install(packages)
  xml = Builder::XmlMarkup.new indent: 2
  xml.instruct!

  xml.packages {
    packages.each { |app|
      xml.package({ id: app[:package_name], version: app[:version] == "latest" ? nil : app[:version] })
    }
  }

  save_to_file(@settings[:package_file], xml.target!)
end

def get_list_of_installed_apps__chocolatey
  `choco list --no-color -lo`.encode("utf-8", invalid: :replace, undef: :replace, replace: "?").split("\n")[1..-1].each { |line|
    if line.match(/.+(?:\d|\.|\-)+\d$/i)
      line_splitted = line.split("\s")
      @settings[:installed_packages][:chocolatey].push({ name: line_splitted.first, version: line_splitted.last })
    end
  }
  @settings[:installed_packages][:chocolatey] = @settings[:installed_packages][:chocolatey].sort_by do |package| package[:name] end
end

def get_list_of_installed_apps__other
  keys = [
    "HKLM:SOFTWARE/WOW6432Node/Microsoft/Windows/CurrentVersion/Uninstall",
    "HKLM:SOFTWARE/Microsoft/Windows/CurrentVersion/Uninstall",
  ]
  installed_apps_chocolatey = @settings[:installed_packages][:chocolatey].map(&:values).map(&:first).map(&:downcase)
  installed_apps_others = []
  keys.each do |key|
    installed_apps_others << external_command("powershell -Command \" & {Get-ItemProperty #{win_path key}\\* | Select-Object DisplayName, DisplayVersion, UninstallString | ConvertTo-Csv -Delimiter \'#\' -NoTypeInformation | Select-Object -Skip 1}\"", true)
  end
  installed_apps_others = installed_apps_others.map do |key_output|
    key_output.split("\n")
  end.flatten.reject do |line|
    line == "##"
  end.map do |app|
    app.split("#")
  end.each do |app|
    display_name, version, uninstall_string = app
    display_name = display_name.delete('"')
    version = version.delete('"')
    version = "unknown" if version.chomp == ""

    if installed_apps_chocolatey.select do |cap|
      display_name.downcase.include? cap
    end.any? || uninstall_string.kind_of?(NilClass)
      next
    end

    @settings[:installed_packages][:other].push({ name: display_name, version: version, uninstall_string: uninstall_string }) unless
      @settings[:installed_packages][:other].select { |k| k.slice(:name, :version) == { name: display_name, version: version } }.any?
  end
  @settings[:installed_packages][:other] = @settings[:installed_packages][:other].sort_by do |package| package[:name] end
end

# Gathers list of installed applications trough chocolatey wind windows registry
def get_list_of_installed_apps(only_chocolatey = true)
  message "Getting list of installed apps", :command unless only_chocolatey

  @settings[:installed_packages] = {
    chocolatey: [],
    other: [],
  }

  if only_chocolatey
    get_list_of_installed_apps__chocolatey
  else
    get_list_of_installed_apps__chocolatey
    get_list_of_installed_apps__other
  end
end

# Reports list of installed applications into server
def report_list_of_installed_apps
  get_list_of_installed_apps(false)
  begin
    installed_packages = []
    @settings[:installed_packages].each_pair do |type, packages|
      installed_packages << packages.map { |package| package.merge({ app_type: type }) }
    end
    message "Sending report to server", :command
    report = RestClient.post(
      @settings[:server_url] + "/api/v1/nodes/" + @settings[:node_auth_id], {
        hostname: @settings[:node_name],
        installed_packages: installed_packages.flatten,
      }.to_json, { content_type: :json, accept: :json }
    )
  rescue => e
    errors(e)
  end
end

def check_need_variables(variable)
  error "Settings doesn't contain #{variable.upcase}!" unless @settings[variable]
end

# Updates client to latest available version on server
def self_update
  begin
    # Check target version specified by server
    target_client_version = JSON.parse(RestClient.get(@settings[:server_url] + "/downloads/version.json"), { :symbolize_names => true })[:client]
    current_client_version = @settings[:version]

    message "Checking client version", :command
    if target_client_version.nil?
      error ["ERROR: Can't find target client version", "Check your connection!"]
    end

    # Compare both versions and update client if they differ
    if current_client_version != target_client_version
      message "New version available - Updating Client", :command

      # * Download new version of client
      temporary_file = RestClient::Request.execute(
        method: :get,
        url: @settings[:server_url] + "/downloads/" + target_client_version + ".exe",
        raw_response: true,
      )
      FileUtils.cp(temporary_file.file.path, File.join(Dir.pwd, "client_#{target_client_version}.exe"))
      external_command "start \"#{@settings[:client_name]}\" cmd /q /c \"echo Updating client in.. && timeout 5 && move client_*.exe client.exe\""
      exit
    else
      message "version up to date"
    end
  rescue => e
    errors(e)
  end
end
